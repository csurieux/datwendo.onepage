﻿using Datwendo.OnePage.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;

namespace Datwendo.OnePage.Drivers {
    public class OnePageSettingsPartDriver : ContentPartDriver<OnePageSettingsPart> {
        private const string TemplateName = "Parts/OnePageSettings";
        public OnePageSettingsPartDriver() {
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override string Prefix {
            get { return "OnePageSettings"; }
        }

        protected override DriverResult Editor(OnePageSettingsPart part, dynamic shapeHelper) {
            return ContentShape("Parts_OnePageSettings_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix))
                .OnGroup("OnePage");
        }

        protected override DriverResult Editor(OnePageSettingsPart part, IUpdateModel updater, dynamic shapeHelper) {
            if (updater.TryUpdateModel(part, Prefix, null, null)) {

            }
            return Editor(part, shapeHelper);
        }

        protected override void Importing(OnePageSettingsPart part, ImportContentContext context) {
            var elementName = part.PartDefinition.Name;
        }

        protected override void Exporting(OnePageSettingsPart part, ExportContentContext context) {
            var el = context.Element(part.PartDefinition.Name);
        }
    }
}