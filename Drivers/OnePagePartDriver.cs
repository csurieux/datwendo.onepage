﻿using Datwendo.OnePage.Models;
using Datwendo.OnePage.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;
using System;

namespace Datwendo.OnePage.Drivers {
    public class OnePagePartDriver : ContentPartDriver<OnePagePart> {
        private const string TemplateName = "Parts/OnePagePart";
        private readonly IOrchardServices _orchardServices;
        public OnePagePartDriver(IOrchardServices orchardServices) {
            _orchardServices = orchardServices;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override string Prefix {
            get { return "OnePagePart"; }
        }

        protected override DriverResult Display(OnePagePart part, string displayType, dynamic shapeHelper) {
            var settings = _orchardServices.WorkContext.CurrentSite.As<OnePageSettingsPart>();
            // Display nothing but admin in operation mode
            if (settings == null || settings.OperationMode) {
                return ContentShape("Parts_OnePagePart_SummaryAdmin",
                                    () => shapeHelper.Parts_OnePagePart_SummaryAdmin(part));
            }
           // OnePageViewModel model = new OnePageViewModel { Part = part, Settings = settings };
            return Combined(
                ContentShape("Parts_OnePagePart",
                    () => shapeHelper.Parts_OnePagePart(Part: part, Settings: settings)),
                ContentShape("Parts_OnePagePart_SummaryAdmin",
                    () => shapeHelper.Parts_OnePagePart_SummaryAdmin(part))
                );
        }

        protected override DriverResult Editor(OnePagePart part, dynamic shapeHelper) {
            return ContentShape("Parts_OnePagePart_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: Prefix));
        }

        protected override DriverResult Editor(OnePagePart part, IUpdateModel updater, dynamic shapeHelper) {
            updater.TryUpdateModel(part, Prefix, null, null);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(OnePagePart part, ImportContentContext context) {
            base.Importing(part, context);

            throw new NotImplementedException();
        }
        protected override void Exporting(OnePagePart part, ExportContentContext context) {
            base.Exporting(part, context);

            throw new NotImplementedException();
        }
    }
}