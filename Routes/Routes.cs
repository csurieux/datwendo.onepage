﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Mvc.Routes;

namespace Datwendo.OnePage.Routes {
    public class Routes : IRouteProvider {
        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Priority = 10,
                    Name= "OnePage",
                    Route = new Route(
                        "OnePage",
                        new RouteValueDictionary {
                            {"area", "Datwendo.OnePage"},
                            {"controller", "OnePage"},
                            {"action", "Accept"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Datwendo.OnePage"}
                        },
                        new MvcRouteHandler())
                },

            };
        }
    }
}