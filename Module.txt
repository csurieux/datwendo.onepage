﻿Name: Datwendo.OnePage
AntiForgery: enabled
Author: Christian Surieux
Website: https://www.datwendo.co
Version: 1.0
OrchardVersion: 1.9
Description: Utilities for site maintenance mode
Features:
    Datwendo.OnePage:
        Description: Define a contentitem to use as homepage content, provides Maintenace mode for the site.
		Category: Design
    Datwendo.OnePage.Layers.Extension:
        Description: Define a contentitem to use as homepage content, provides Maintenace mode for the site.
		Category: Design
		Dependencies: Datwendo.OnePage, Orchard.Widgets
