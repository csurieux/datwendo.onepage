﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.FieldStorage.InfosetStorage;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datwendo.OnePage.Models {
    public class OnePagePart  : ContentPart {

        public string Text {
            get {
                return this.As<InfosetPart>().Get<OnePagePart>("Text", null);
            }
            set {
                this.As<InfosetPart>().Set<OnePageSettingsPart>("Text", null, value);
            }
        }
    }
}
