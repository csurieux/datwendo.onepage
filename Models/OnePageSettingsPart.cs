﻿using Orchard.ContentManagement;
using Orchard.ContentManagement.FieldStorage.InfosetStorage;
using System;
using System.Globalization;

namespace Datwendo.OnePage.Models {

    public class OnePageSettingsPart : ContentPart {
        public bool OperationMode {
            get {
                return Convert.ToBoolean(this.As<InfosetPart>().Get<OnePageSettingsPart>("OperationMode", null), CultureInfo.InvariantCulture);
            }
            set {
                this.As<InfosetPart>().Set<OnePageSettingsPart>("OperationMode", null, Convert.ToString(value, CultureInfo.InvariantCulture));
            }
        }

        public bool FilterAllUrls {
            get {
                return Convert.ToBoolean(this.As<InfosetPart>().Get<OnePageSettingsPart>("FilterAllUrls", null), CultureInfo.InvariantCulture);
            }
            set {
                this.As<InfosetPart>().Set<OnePageSettingsPart>("FilterAllUrls", null, Convert.ToString(value, CultureInfo.InvariantCulture));
            }
        }
        /* not implemented
        public string Urls {
            get {
                return this.As<InfosetPart>().Get<OnePageSettingsPart>("Urls");
            }
            set {
                this.As<InfosetPart>().Set<OnePageSettingsPart>("Urls", value);
            }
        }
        */
        public string Text {
            get {
                return this.As<InfosetPart>().Get<OnePageSettingsPart>("Text");
            }
            set {
                this.As<InfosetPart>().Set<OnePageSettingsPart>("Text", value);
            }
        }
    }
}
