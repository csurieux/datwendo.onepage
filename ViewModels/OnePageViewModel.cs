﻿using Datwendo.OnePage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Datwendo.OnePage.ViewModels {
    public class OnePageViewModel {
        public OnePagePart Part { get; set; }
        public OnePageSettingsPart Settings { get; set; }
    }
}