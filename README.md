# Datwendo.OnePage is an Orchard 1.9.x utility module  #

* 2 modes : operation and maintenance
* The OnePage Item is here to use as a dummy homme page when you have no special content (or very dedicated content), but widgets to display on Home
* The OnePagePart in operation mode displays nothing
* The OnePagePart in maintenance mode displays the Html text you define in settings and a couple of 2 filters keep the homepage as the only accessible one
* Adapt the corresponding template in your theme
* Version 1.0


* Christian Surieux - Datwendo - 2016
* Apache v2 license