using System;
using System.Collections.Generic;
using System.Data;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.Core.Contents.Extensions;
using Orchard.Data.Migration;

namespace Datwendo.OnePage {
    public class Migrations : DataMigrationImpl {

        public int Create() {
            
            ContentDefinitionManager.AlterPartDefinition("OnePagePart",
                builder => builder.Attachable()
                                .WithDescription("Mainly to use one time in OnePageContent but you can add OnePagePart to any content for which you want to count access"));

            ContentDefinitionManager.AlterTypeDefinition("OnePage", cfg => cfg
                .WithPart("OnePagePart")
                .WithPart("TitlePart")
                .WithPart("AutoroutePart", builder => builder
                    .WithSetting("AutorouteSettings.AllowCustomPattern", "true")
                    .WithSetting("AutorouteSettings.AutomaticAdjustmentOnEdit", "false")
                    .WithSetting("AutorouteSettings.PatternDefinitions", "[{Name:'Title', Pattern: 'OnePage/{Content.Slug}', Description: 'my-OnePage'}]")
                    .WithSetting("AutorouteSettings.DefaultPatternIndex", "0"))
                .WithPart("CommonPart", p => p
                    .WithSetting("DateEditorSettings.ShowDateEditor", "false"))
                .WithIdentity()
                .WithSetting("OnePagePartSettings.OperationMode", "true")
                .WithSetting("OnePagePartSettings.FilterAllUrls", "true")
                .Creatable()
                .Listable()
                .Draftable(false)
                .DisplayedAs("OnePage Content to set as HomePage"))
                ;

            return 1;
        }
    }
}