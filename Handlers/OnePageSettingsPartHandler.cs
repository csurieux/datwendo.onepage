﻿using Datwendo.OnePage.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Localization;

namespace Datwendo.OnePage.Handlers {
    public class OnePageSettingsPartHandler : ContentHandler {
        public OnePageSettingsPartHandler() {
            T = NullLocalizer.Instance;
            Filters.Add(new ActivatingFilter<OnePageSettingsPart>("Site"));
        }

        public Localizer T { get; set; }

        protected override void GetItemMetadata(GetContentItemMetadataContext context) {
            if (context.ContentItem.ContentType != "Site")
                return;
            base.GetItemMetadata(context);
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(T("One Page")) {
                Id = "OnePage",
                Position = "5"
            });
        }
    }
}
