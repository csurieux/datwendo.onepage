﻿using System;
using System.Web.Mvc;

namespace Datwendo.OnePage.Controllers {

    public class OnePageController : Controller {
        public OnePageController() {
        }

        [HttpPost]
        public JsonResult Accept(string referrer) {
            return new JsonResult { Data = string.Format("Ok-{0:u}",DateTime.Now) };
        }
    }
}