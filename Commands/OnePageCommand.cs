﻿using System;
using System.Collections.Generic;
using Orchard.Commands;
using Orchard.ContentManagement;
using Datwendo.OnePage.Models;

namespace Orchard.OnePage.Commands {
    public class OnePagesCommand : DefaultOrchardCommandHandler {
        private readonly IOrchardServices _services;

        public OnePagesCommand(IOrchardServices services) {
            _services = services;
        }

        [OrchardSwitch]
        public bool OperationMode { get; set; }
        [OrchardSwitch]
        public bool FilterAllUrls { get; set; }
        [OrchardSwitch]
        public string Text { get; set; }

        [CommandName("site onepage set OnePage")]
        [CommandHelp("site setting set OnePage /OperationMode:false /FilterAllUrls:true /Text:<value> \r\n")]
        [OrchardSwitches("OperationMode,FilterAllUrls,Urls,Text")]
        public void SetOnePageInfo() {
            var settings = _services.WorkContext.CurrentSite.As<OnePageSettingsPart>();
            if (settings == null) {
                return;
            }

            settings.OperationMode = OperationMode;
            settings.FilterAllUrls = FilterAllUrls;
            settings.Text = Text;
            
            Context.Output.WriteLine(T("'OperationMode' site setting set to '{0}'", OperationMode));
            Context.Output.WriteLine(T("'FilterAllUrls' site setting set to '{0}'", FilterAllUrls));
            Context.Output.WriteLine(T("'Text' site setting set to '{0}'", Text));
        }
    }
}