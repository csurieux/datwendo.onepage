﻿using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datwendo.OnePage.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;
using Orchard.Localization;
using Orchard.Mvc.Filters;
using Orchard.UI.Admin;
using Orchard.UI.Admin.Notification;
using Orchard.UI.Notify;
using System;
using System.Collections.Generic;
using Orchard.UI.Zones;
using Orchard.DisplayManagement.Shapes;
using Orchard.Autoroute.Services;

namespace Datwendo.OnePage.Filters {
    public class OnePageActionFilter : FilterProvider, IActionFilter {
        public readonly IHomeAliasService _homeAliasService;
        private readonly IOrchardServices _orchardServices;

        public Localizer T { get; set; }

        public OnePageActionFilter( IOrchardServices orchardServices
            , IShapeFactory shapefactory
            , IHomeAliasService homeAliasService) {
            _homeAliasService = homeAliasService;
            _orchardServices = orchardServices;
            T = NullLocalizer.Instance;
        }

        public void OnActionExecuted(ActionExecutedContext filterContext) {
            var settingsPart = _orchardServices.WorkContext.CurrentSite.As<OnePageSettingsPart>();

            /*if (!settings.Enabled) {
                _orchardServices.Notifier.Warning(T("You need to configure the SSL settings."));
            }*/
        }

        public void OnActionExecuting(ActionExecutingContext filterContext) {
            if (AdminFilter.IsApplied(filterContext.RequestContext))
                return;

            var settingsPart = _orchardServices.WorkContext.CurrentSite.As<OnePageSettingsPart>();
            if (settingsPart.OperationMode)
                return;

            var request = filterContext.HttpContext.Request;
            
            var homePageRt = _homeAliasService.GetHomeRoute();
            var homeUrl = new UrlHelper(request.RequestContext).RouteUrl(homePageRt);
            var path = request.Url.GetLeftPart(UriPartial.Path).Substring(request.Url.GetLeftPart(UriPartial.Authority).Length);

            // Already homepage don't redirect
            if ( string.Equals(path,homeUrl,StringComparison.InvariantCultureIgnoreCase) )
                return;

            if (!settingsPart.FilterAllUrls)
                return;
            // Do a redirect to homepage
            filterContext.Result = new RedirectResult("~/");
            return;
        }
    }
    public class OnePageResultFilter : FilterProvider, IResultFilter {

        private readonly IWorkContextAccessor _workContextAccessor;
        
        public Localizer T { get; set; }

        public OnePageResultFilter(IWorkContextAccessor workContextAccessor) {
            _workContextAccessor = workContextAccessor;
            T = NullLocalizer.Instance;
        }

        public void OnResultExecuted(ResultExecutedContext filterContext) {
            var settingsPart = _workContextAccessor.GetContext().CurrentSite.As<OnePageSettingsPart>();
        }

        public void OnResultExecuting(ResultExecutingContext filterContext) {
            if (AdminFilter.IsApplied(filterContext.RequestContext))
                return;

            var settingsPart = _workContextAccessor.GetContext().CurrentSite.As<OnePageSettingsPart>();
            if (settingsPart.OperationMode)
                return;

            var context = _workContextAccessor.GetContext(filterContext);
            Func<dynamic> factory = () => new Shape();
            var oldLayout = context.Layout;
            context.Layout = new ZoneHolding(factory);
            context.Layout.Tail = oldLayout.Tail;
            context.Layout.Body = oldLayout.Body;
            // Get rid of any Zone which could contain content but Content, Navigation and Message
            context.Layout.Navigation = oldLayout.Navigation;
            context.Layout.Navigation.Parent = context.Layout;
            context.Layout.Head = oldLayout.Head;
            context.Layout.Metadata = oldLayout.Metadata;
            context.Layout.Content = oldLayout.Content;
            context.Layout.Messages = oldLayout.Messages;
        }
    }
}